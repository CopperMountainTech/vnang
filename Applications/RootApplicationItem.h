#ifndef ROOTAPPLICATIONITEM_H
#define ROOTAPPLICATIONITEM_H

#include <QXmlStreamWriter>

#include <RootBaseItem.h>
#include <UUidCommon.h>
#include <AnalyzerCommand.h>
#include <StackUndo/StackUndoDispatcher.h>
#include <AutomationItem.h>
#include <VnaAnalyzer.h>
#include <DeviceManager.h>
#include <DevicePool.h>

namespace Planar {

class RootApplicationItem : public Planar::RootBaseItem
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)

    Q_PROPERTY(Planar::Automation::AutomationItem* automationItem READ automationItem NOTIFY
               rootPropertyAdded)
    Q_PROPERTY(Planar::VNA::Analyzer::VnaAnalyzer* analyzer READ analyzer NOTIFY rootPropertyAdded)
public:
    RootApplicationItem();
    ~RootApplicationItem() override;

    AnalyzerWriteStream* streamWriter() override
    {
        return &_streamWriterAnalyzer;
    }

    Planar::Automation::AutomationItem* automationItem()
    {
        return &_automationItem;
    }

    Planar::VNA::Analyzer::VnaAnalyzer* analyzer()
    {
        return _analyzer;
    }

    Planar::Hardware::DeviceManager* deviceManager()
    {
        return _deviceManager;
    }

    Q_INVOKABLE void stackStepBack();
    Q_INVOKABLE void stackStepForward();

    Q_INVOKABLE void requestInitialize();

protected:
    AnalyzerWriteStream _streamWriterAnalyzer;
    Planar::Automation::AutomationItem _automationItem;
    Planar::VNA::Analyzer::VnaAnalyzer* _analyzer;
    Planar::Hardware::DeviceManager* _deviceManager;

protected slots:
    void onDeviceConnectionChanged(Planar::Hardware::DevicePool* pool,
                                   Planar::Hardware::DeviceBase* device, bool isAppend);
};

} //namespace Planar

#endif //ROOTAPPLICATIONITEM_H



