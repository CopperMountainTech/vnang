﻿#include "RootApplicationItem.h"

#include <RootBaseItem.h>
#include <VnaHw.h>

namespace Planar {

RootApplicationItem::RootApplicationItem() : RootBaseItem(),
    _streamWriterAnalyzer(), _automationItem(this)
{
    //  qRegisterMetaType<Planar::VNA::Hardware::IBinder>();
    _analyzer = Planar::VNA::Analyzer::VnaAnalyzer::createInstance(this);
    _deviceManager = new Planar::Hardware::DeviceManager(this);
    connect(_deviceManager, &Planar::Hardware::DeviceManager::connectionChanged, this,
            &RootApplicationItem::onDeviceConnectionChanged);
}

RootApplicationItem::~RootApplicationItem()
{
    delete _deviceManager;
}

void RootApplicationItem::stackStepBack()
{
    undoDispatcher()->undoLastCommand();
    //_undoDispatcher.undoCommandToIndex(1);
}


void RootApplicationItem::stackStepForward()
{
    undoDispatcher()->redoLastCommand();
}


void RootApplicationItem::onDeviceConnectionChanged(Planar::Hardware::DevicePool* /*pool*/,
                                                    Planar::Hardware::DeviceBase* /*device*/, bool /*isAppend*/)
{
//    qDebug() << isAppend;

//
//    if (_analyzer->isInitialized() && _analyzer->vna()->connected())
//        return;

    // какая то логика для выбора инстанс
    Planar::VNA::Hardware::IVna* vna = static_cast<Planar::VNA::Hardware::IVna*>
                                       (_deviceManager->deviceInstance());

    if (_deviceManager->select(vna, true)) { // lock
        _analyzer->requestInitializeNewDevice(vna);
    }
}


void RootApplicationItem::requestInitialize()
{
    Planar::VNA::Hardware::IVna* vna = static_cast<Planar::VNA::Hardware::IVna*>
                                       (_deviceManager->deviceInstance());

    bool isAttach = !vna->connected();
    vna->setConnected(
        isAttach);                    // имитация подключения/отключения
    onDeviceConnectionChanged(vna->pool(), vna, isAttach);
}

} //namespace Planar
